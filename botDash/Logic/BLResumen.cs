﻿using System;
using System.Collections.Generic;
// using botDash.Util;
using botPortal.Abstracciones;
using botPortal.Datos;
using botPortal.Model;
using botPortal.Util;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace botPortal.Logic
{
    public class BLResumen : IResumen
    {
        private readonly DAResumen _daResumen;
        private readonly IHelper _helper;
        private readonly IConfigurationRoot _configurationRoot;

           public void consultar()
        {
            Log.Information("Iniciando proceso carga");
            try
            {
                // Resumen entidad = _daResumen.listar();
                // bool registro = _daResumen.existe(entidad.fecha);
                // if (!registro)
                // {
                //     _daResumen.registrar(entidad);
                //     entidad.fecha = "FALTAN";
                //     entidad.descripcion = entidad.cantidad - entidad.descripcion;
                //     entidad.disponibilidad = entidad.cantidad - entidad.disponibilidad;
                //     entidad.firma = entidad.cantidad - entidad.firma;
                //     entidad.imagen = entidad.cantidad - entidad.imagen;
                //     entidad.mercado_pago = entidad.cantidad - entidad.mercado_pago;
                //     entidad.monto_consultorio = entidad.cantidad - entidad.monto_consultorio;
                //     entidad.monto_teleconsulta = entidad.cantidad - entidad.monto_teleconsulta;
                //     _daResumen.actualizar(entidad);
                // }
                // Log.Information("Terminó proceso carga");
                // return true;
                Log.Information("Iniciando el console log");
            }
            catch (Exception ex)
            {
                Log.Error("Falló proceso carga");
                Log.Error(ex.Message);
                _helper.logSentryIO(ex);
            }
        }

        // public void correos()
        // {
        //     try
        //     {
        //         Log.Information("Iniciando proceso Correos");
        //         string asunto = _configurationRoot.GetSection("Onboarding").GetSection("Asunto_correo").Value;
        //         List<CorreoLista> data = _daResumen.onboarding();
        //         List<string> casiListo1 = new List<string>();
        //         List<string> casiListo2 = new List<string>();
        //         List<string> casiListo3 = new List<string>();
        //         List<string> casiListo4 = new List<string>();

        //         foreach (CorreoLista item in data)
        //         {
        //             //0000
        //             if (item.paso1 == 0 && item.paso2 == 0 && item.paso3 == 0 && item.paso4 == 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado0000));
        //             }
        //             //1000
        //             else if (item.paso1 > 0 && item.paso2 == 0 && item.paso3 == 0 && item.paso4 == 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1000));
        //             }
        //             //1100
        //             else if (item.paso1 > 0 && item.paso2 > 0 && item.paso3 == 0 && item.paso4 == 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1100));
        //             }
        //             //1110
        //             else if (item.paso1 > 0 && item.paso2 > 0 && item.paso3 > 0 && item.paso4 == 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1110));
        //             }
        //             //1010
        //             else if (item.paso1 > 0 && item.paso2 == 0 && item.paso3 > 0 && item.paso4 == 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1010));
        //             }
        //             //1001
        //             else if (item.paso1 > 0 && item.paso2 == 0 && item.paso3 == 0 && item.paso4 > 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1001));
        //             }
        //             //1101
        //             else if (item.paso1 > 0 && item.paso2 > 0 && item.paso3 == 0 && item.paso4 > 0)
        //             {
        //                 _correo.enviar(_configurationRoot.GetSection("MailGun")
        //                   .GetSection("MAILGUN_FROM").Value, item.correo, string.Empty,
        //                   string.Empty, asunto,
        //                   CorreosTemplate.paso1(
        //                   _configurationRoot.GetSection("Onboarding").GetSection("MEL_S3").Value,
        //                   _configurationRoot.GetSection("Onboarding").GetSection("Paso1").Value,
        //                   EstadoOnboarding.Estado1101));
        //             }
        //         }
        //         Log.Information("Teminó proceso Correos");
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Error("Falló proceso Correos");
        //         _helper.logSentryIO(ex);
        //     }
        // }

    }
}
