﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace botPortal.Model
{
    public class CorreoLista
    {
        public string correo { get; set; }
        public int paso1 { get; set; }
        public int paso2 { get; set; }
        public int paso3 { get; set; }
        public int paso4 { get; set; }
    }
}