﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using botPortal.Model;
using botPortal.Util;
using Microsoft.Extensions.Configuration;
using MySql.Data;
using MySql.Data.MySqlClient;
using Serilog;

namespace botPortal.Datos
{
    public class DAResumen
    {
        private readonly IConfigurationRoot _configurationRoot;
        private readonly IHelper _helper;

        public DAResumen(IConfigurationRoot configurationRoot, IHelper helper)
        {
            _configurationRoot = configurationRoot;
            _helper = helper;
        }

        // public Resumen listar()
        // {
        //     Resumen entidad = new Resumen();
        //     try
        //     {
        //         using (MySqlConnection conn = new MySqlConnection(_configurationRoot.GetSection("ConnectionStrings:base").Value))
        //         {
        //             conn.Open();
        //             string sql = "";

        //             sql = @"SELECT 	date_format(NOW(),'%Y-%m-%d') AS fecha,
        //                             SUM(case
		// 	                        when	IFNULL(medico.mel_vch_descripcion,'') = '' then 0
		// 	                        ELSE 	1
		// 	                        END)	AS descripcion,
		// 	                        sum(case
		// 	                        when	IFNULL(medico.mel_vch_imagen,'') = '' then 0
		// 	                        ELSE 	1
		// 	                        END)	AS imagen,
		// 	                        sum(case
		// 	                        when	IFNULL(medico.mel_vch_firma,'') = '' then 0
		// 	                        ELSE 	1
		// 	                        END)	AS firma,
		// 	                        sum(case
		// 	                        when	IFNULL((SELECT	mel_dec_monto
		// 			                        FROM		tbl_mel_medicotarifario
		// 			                        WHERE		mel_int_tipocita = 1
		// 			                        AND		mel_tin_estado = 1
		// 			                        AND		medico.mel_int_idmedico = mel_int_idmedico
		// 			                        AND		mel_dec_monto > 0),0) = 0 then 0
		// 	                        ELSE	1
		// 	                        END) AS	monto_teleconsulta,
		// 	                        sum(case
		// 	                        when	IFNULL((SELECT	mel_dec_monto
		// 			                        FROM	tbl_mel_medicotarifario
		// 			                        WHERE	mel_int_tipocita = 2
		// 			                        AND		mel_tin_estado = 1
		// 			                        AND		medico.mel_int_idmedico = mel_int_idmedico
		// 			                        AND		mel_dec_monto > 0),0) = 0 then 0
		// 	                        ELSE	1
		// 	                        END) AS	monto_consultorio,
		// 	                        sum(case
		// 	                        when	(SELECT COUNT(*) FROM tbl_mel_medicohorario horario WHERE mel_tin_estado = 1 AND medico.mel_int_idmedico = horario.mel_int_idmedico) = 0 then 0
		// 	                        ELSE 1
		// 	                        end) AS disponibilidad,
		// 	                        sum(case
		// 	                        when	ifnull(usuario.mel_vch_token,'') = '' then 0
		// 	                        ELSE 1
		// 	                        END) AS mercado_pago,
		// 	                        COUNT(*) as cantidad
        //                 FROM 		tbl_mel_medico medico
        //                 INNER JOIN tbl_mel_usuario usuario
	    //                     ON		medico.mel_int_idmedico = usuario.mel_int_idusuario ";

        //             MySqlCommand cmd = new MySqlCommand(sql, conn);
        //             cmd.CommandType = CommandType.Text;
        //             MySqlDataReader reader = cmd.ExecuteReader();

        //             if (reader.Read())
        //             {
        //                 entidad = new Resumen()
        //                 {
        //                     fecha = reader["fecha"].ToString(),
        //                     descripcion = int.Parse(reader["descripcion"].ToString()),
        //                     disponibilidad = int.Parse(reader["disponibilidad"].ToString()),
        //                     imagen = int.Parse(reader["descripcion"].ToString()),
        //                     firma = int.Parse(reader["disponibilidad"].ToString()),
        //                     mercado_pago = int.Parse(reader["descripcion"].ToString()),
        //                     monto_consultorio = int.Parse(reader["disponibilidad"].ToString()),
        //                     monto_teleconsulta = int.Parse(reader["descripcion"].ToString()),
        //                     cantidad = int.Parse(reader["cantidad"].ToString())
        //                 };
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         _helper.logSentryIO(ex);
        //         return null;
        //     }

        //     return entidad;
        // }

        // public bool existe(string fecha)
        // {
        //     bool registro = false;
        //     try
        //     {
        //         using (MySqlConnection conn = new MySqlConnection(_configurationRoot.GetSection("ConnectionStrings:base").Value))
        //         {
        //             conn.Open();
        //             string sql = "";

        //             sql = @"SELECT 	count(*) as cantidad
        //                     FROM 	tbl_dsh_resumenactivacion
        //                     where   dsh_vch_fecha = '" + fecha + "'";

        //             MySqlCommand cmd = new MySqlCommand(sql, conn);
        //             cmd.CommandType = CommandType.Text;
        //             MySqlDataReader reader = cmd.ExecuteReader();

        //             while (reader.Read())
        //             {
        //                 registro = int.Parse(reader["cantidad"].ToString()) == 0 ? false : true;
        //             }
        //             return registro;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         _helper.logSentryIO(ex);
        //         return false;
        //     }
        // }

        // public void registrar(Resumen item)
        // {
        //     using (MySqlConnection conn = new MySqlConnection(_configurationRoot.GetSection("ConnectionStrings:base").Value))
        //     {
        //         conn.Open();
        //         try
        //         {
        //             var sql = @"insert into tbl_dsh_resumenactivacion
        //                         (
		// 					        dsh_vch_fecha,
        //                             dsh_int_descripcion,
        //                             dsh_int_disponibilidad,
        //                             dsh_int_imagen,
        //                             dsh_int_firma,
        //                             dsh_int_mercadopago,
        //                             dsh_int_montoconsultorio,
        //                             dsh_int_montoteleconsulta,
        //                             dsh_dat_crea
		// 				        )
		// 				       values (@fecha, @descripcion, @disponibilidad, @imagen, @firma, @mercadopago, @montoconsultorio, @montoteleconsulta, now()) ";
        //             MySqlCommand cmd = conn.CreateCommand();
        //             cmd.CommandText = sql;
        //             cmd.Parameters.AddWithValue("@fecha", item.fecha);
        //             cmd.Parameters.AddWithValue("@descripcion", item.descripcion);
        //             cmd.Parameters.AddWithValue("@disponibilidad", item.disponibilidad);
        //             cmd.Parameters.AddWithValue("@imagen", item.imagen);
        //             cmd.Parameters.AddWithValue("@firma", item.firma);
        //             cmd.Parameters.AddWithValue("@mercadopago", item.mercado_pago);
        //             cmd.Parameters.AddWithValue("@montoconsultorio", item.monto_consultorio);
        //             cmd.Parameters.AddWithValue("@montoteleconsulta", item.monto_teleconsulta);
        //             cmd.Connection = conn;
        //             cmd.ExecuteNonQuery();
        //             conn.Close();
        //         }
        //         catch (Exception ex)
        //         {
        //             _helper.logSentryIO(ex);
        //         }
        //     }
        // }

        // public void actualizar(Resumen item)
        // {
        //     using (MySqlConnection conn = new MySqlConnection(_configurationRoot.GetSection("ConnectionStrings:base").Value))
        //     {
        //         conn.Open();
        //         try
        //         {
        //             var sql = @"update  tbl_dsh_resumenactivacion
        //                         set     dsh_int_descripcion = @descripcion,
        //                                 dsh_int_disponibilidad = @disponibilidad,
        //                                 dsh_int_imagen = @imagen,
        //                                 dsh_int_firma = @firma,
        //                                 dsh_int_mercadopago = @mercadopago,
        //                                 dsh_int_montoconsultorio = @montoconsultorio,
        //                                 dsh_int_montoteleconsulta = @montoteleconsulta,
        //                                 dsh_dat_crea = DATE_ADD(NOW(), INTERVAL 10 SECOND)
		// 				       where    dsh_vch_fecha = @fecha";
        //             MySqlCommand cmd = conn.CreateCommand();
        //             cmd.CommandText = sql;
        //             cmd.Parameters.AddWithValue("@fecha", item.fecha);
        //             cmd.Parameters.AddWithValue("@descripcion", item.descripcion);
        //             cmd.Parameters.AddWithValue("@disponibilidad", item.disponibilidad);
        //             cmd.Parameters.AddWithValue("@imagen", item.imagen);
        //             cmd.Parameters.AddWithValue("@firma", item.firma);
        //             cmd.Parameters.AddWithValue("@mercadopago", item.mercado_pago);
        //             cmd.Parameters.AddWithValue("@montoconsultorio", item.monto_consultorio);
        //             cmd.Parameters.AddWithValue("@montoteleconsulta", item.monto_teleconsulta);
        //             cmd.Connection = conn;
        //             cmd.ExecuteNonQuery();
        //             conn.Close();
        //         }
        //         catch (Exception ex)
        //         {
        //             _helper.logSentryIO(ex);
        //         }
        //     }
        // }

        // public List<CorreoLista> onboarding()
        // {
        //     List<CorreoLista> lista = new List<CorreoLista>();
        //     try
        //     {
        //         using (MySqlConnection conn = new MySqlConnection(_configurationRoot.GetSection("ConnectionStrings:base").Value))
        //         {
        //             conn.Open();
        //             string sql = "";

        //             sql = @"SELECT 	usuario.mel_vch_correo as correo,
        //                             case
		// 	                        when	IFNULL(medico.mel_vch_descripcion,'') <> '' and IFNULL(medico.mel_vch_imagen,'') <> '' then 1
		// 	                        ELSE 	0
		// 	                        END	AS paso1,
		// 	                        (SELECT	COUNT(*)
		// 			                FROM	tbl_mel_medicotarifario tarifa
		// 			                WHERE	tarifa.mel_int_tipocita = 1
		// 			                AND		tarifa.mel_tin_estado = 1
		// 			                AND		medico.mel_int_idmedico = tarifa.mel_int_idmedico
		// 			                AND		tarifa.mel_dec_monto > 0) AS paso2,
		// 	                        (SELECT COUNT(*) FROM tbl_mel_medicohorario horario WHERE horario.mel_int_tipocita = 1 and horario.mel_tin_estado = 1 AND medico.mel_int_idmedico = horario.mel_int_idmedico) 
		// 	                        AS paso3,
		// 	                        case
		// 	                        when	ifnull(usuario.mel_vch_token,'') <> '' then 1
		// 	                        ELSE 0
		// 	                        END  AS paso4
        //                 FROM 		tbl_mel_medico medico
        //                 INNER JOIN tbl_mel_usuario usuario
	    //                     ON		medico.mel_int_idmedico = usuario.mel_int_idusuario ";

        //             if (_configurationRoot.GetSection("MailGun:Prueba").Value == "1")
        //             {
        //                 sql += $"WHERE mel_vch_correo = '{_configurationRoot.GetSection("MailGun:CORREO_PRUEBA").Value}'";
        //             }


        //             MySqlCommand cmd = new MySqlCommand(sql, conn);
        //             cmd.CommandType = CommandType.Text;
        //             MySqlDataReader reader = cmd.ExecuteReader();
        //             while (reader.Read())
        //             {
        //                 lista.Add(new CorreoLista()
        //                 {
        //                     correo = _configurationRoot.GetSection("MailGun").GetSection("Prueba").Value == "1" ?
        //                     _configurationRoot.GetSection("MailGun").GetSection("CORREO_PRUEBA").Value :
        //                     reader["correo"].ToString(),
        //                     paso1 = int.Parse(reader["paso1"].ToString()),
        //                     paso2 = int.Parse(reader["paso2"].ToString()),
        //                     paso3 = int.Parse(reader["paso3"].ToString()),
        //                     paso4 = int.Parse(reader["paso4"].ToString())
        //                 });
        //             }
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         Log.Error("Error al obtener correo lista");
        //         Log.Error(ex.Message);
        //         _helper.logSentryIO(ex);
        //     }

        //     return lista;
        // }

    }
}
