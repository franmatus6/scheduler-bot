﻿using Microsoft.Extensions.Configuration;
using Sentry;
using System;

namespace botPortal.Util
{
    public class Helper : IHelper
    {
        private readonly IConfigurationRoot _configurationRoot;

        public Helper(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }

        public void logSentryIO(Exception ex)
        {
            using (SentrySdk.Init(_configurationRoot.GetSection("sentry").Value))
            {
                ex.Data.Add("Stack trace", ex.StackTrace);
                //SentrySdk.CaptureException(ex);
            }
        }
    }
}
