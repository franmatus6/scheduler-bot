﻿using botPortal.Abstracciones;
using botPortal.Logic;
using Serilog;
using System;
using System.Threading.Tasks;

namespace botPortal
{
    class App
    {
        private readonly IResumen _resumen;

        public App(IResumen resumen)
        {
            _resumen = resumen;
        }

        public void Run(string option)
        {
            switch (option)
            {
                case "consultar":
                    Console.WriteLine("Comenzando");
                    break;
                default:
                    Console.WriteLine("Opciones disponibles: consultar");
                    break;
            }
        }
    }
}
